class ConfigService {

    constructor($rootScope) {
        this.$rootScope = $rootScope;

        this._client = Config.client;
    }

    getNumberOfAboutItems() {
        let numItems = 4;
        if (this._client == "hardenberg") {
            numItems = 3;
        } else if (this._client == "roc" || this._client == "coen") {
            numItems = 2;
        } else if (this._client == "spov") {
            numItems = 5;
        }
        return numItems;
    };

    getUserItemIndex() {
        let itemIndex = 1;
        if (this._client == "hardenberg") {
            itemIndex = 3;
        } else if (this._client == "roc" || this._client == "coen") {
            itemIndex = 2;
        }
        return itemIndex;
    };

    checkShowCountyForm() {
        return this._client == "hardenberg";
    };

    checkShowCorrectInfo() {
        return this._client == "hardenberg";
    };

    checkShowSchoolForm() {
        return this._client == "roc";
    };

    checkShowSpovForm() {
        return this._client == "spov";
    };

    getSortQuestions() {
        return this._client == "verkiezingen" ? "random" : "default";
    };

    checkShowProfilePicture() {
        return this._client == "hardenberg";
    };

    checkShowQuestionPicture() {
        return this._client == "hardenberg";
    };

    static ConfigFactory($rootScope) {
        return new ConfigService($rootScope);
    }
}

ConfigService.ConfigFactory.$inject = ['$rootScope'];

angular.module('coen.services.config', [])
    .factory('ConfigService', ConfigService.ConfigFactory);

export default ConfigService;
