class TwitterService {

    constructor($q, $window, ConfigService) {
        this.$q = $q;
        this.$window = $window;

        this.ConfigService = ConfigService;

        this._userId = null;
        this._accessToken = null;
        this._authorizationResult = false;

        this.showProfilePicture = ConfigService.checkShowProfilePicture();
        this.showQuestionPicture = ConfigService.checkShowQuestionPicture();
    }

    getPostLight(socialPost) {
        var tweet = "https://twitter.com/share?url=" + Config.shareUrl;
        tweet += "&text=" + socialPost.message.message + " " + socialPost.message.description;
        //tweet += "&hashtags=Onderzoek";
        return tweet;
    };

    publishPostLight(tweet) {
        this.$window.open(tweet);
    };

    isReady() {
        return this._authorizationResult;
    };

    getAnswerPost(user, answer) {
        var post = {};
        post.platform = "twitter";
        post.user = user;

        var message = {};
        if (answer) {
            var imageUrl = Config.webUrl + Config.imageUrl + "/backgrounds/1200/statement-" + answer.questionId + ".jpg";
            //convertImgToBase64(imageUrl, function(base64Img) {
            //    var boundary = getBoundary(base64Img);
            //};

            message.message = answer.text;
            message.name = "";
            if (this.showQuestionPicture) message.picture = Config.webUrl + Config.imageUrl + "/backgrounds/1200/statement-" + answer.questionId + ".jpg";
        }

        post.message = message;
        // post.title = profile.titleText;
        return post;
    };

    getProfilePost(user, profile) {
        var post = {};
        post.platform = "twitter";
        post.user = user;

        var message = {};
        if (profile) {
            message.message = profile.shareText;
            message.name = "";
            if (this.showProfilePicture) message.picture = Config.webUrl + Config.profileUrl + profile.label + ".jpg";
            message.description = profile.teaserText;
        }

        post.message = message;
        post.title = profile.titleText;
        return post;
    };

    convertImgToBase64(url, callback, outputFormat) {
        var canvas = document.createElement('CANVAS');
        var ctx = canvas.getContext('2d');
        var img = new Image;
        img.crossOrigin = 'Anonymous';
        img.onload = function () {
            canvas.height = img.height;
            canvas.width = img.width;
            ctx.drawImage(img, 0, 0);
            var dataURL = canvas.toDataURL(outputFormat || 'image/png');
            callback.call(this, dataURL);
            // Clean up
            canvas = null;
        };
        img.src = url;
    };

    getBoundary(base64Img) {
        var boundaryString = '------------------------------';
        var dataUrl = base64Img.substr(base64Img.indexOf("base64,", 0) + 7);

        var cr = "\r\n";
        var boundary = "--" + boundaryString;
        var body = boundary + cr;
        body += "Content-Disposition: form-data; name=\"key\"" + cr + cr;
        body += "key" + cr;
        body += boundary + cr;
        body += "Content-Disposition: form-data; name=\"fileupload\"; filename=\"statement.png\"" + cr;
        body += "Content-Type: image/png" + cr;
        body += "Content-Transfer-Encoding: base64" + cr + cr;
        body += dataUrl + cr;
        body += boundary + "--" + cr;
        return body;
    };

    publishPost(message) {
        var deferred = this.$q.defer();

        OAuth.popup("twitter", (error, result) => {
            if (error) {
                // handle error
            } else {
                result.post({
                    url: '/1.1/statuses/update.json',
                    data: message
                }).done((data) => {
                    deferred.resolve(data);
                });
            }
        });

        return deferred.promise;
    };

    publishPostWithImage(message) {
        var deferred = this.$q.defer();

        OAuth.popup("twitter", (error, result) => {
            if (error) {
                // handle error
            } else {
                result.post({
                    url: '/1.1/statuses/update_with_media.json',
                    data: {status: message.message, media: message.picture},
                    contentType: 'multipart/form-data'
                }).done(function (res) {
//console.log("finally uploaded an image = ", res);
                });
            }
        });

        return deferred.promise;
    };

    clearCache() {
        OAuth.clearCache('twitter');
        this._authorizationResult = false;
    };

    getLatestTweets() {
        var deferred = this.$q.defer();

        this._authorizationResult.get('/1.1/statuses/home_timeline.json').done((data) => {
            deferred.resolve(data)
        });

        return deferred.promise;
    };

    static TwitterFactory($q, $window, ConfigService) {
        return new TwitterService($q, $window, ConfigService);
    }
}

TwitterService.TwitterFactory.$inject = ['$q', '$window', 'ConfigService'];

angular.module('coen.services.twitter', [])
    .factory('TwitterService', TwitterService.TwitterFactory);

export default TwitterService;