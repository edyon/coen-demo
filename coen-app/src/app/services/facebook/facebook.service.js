window.fbAsyncInit = function () {
    FB.init({
        appId: Config.fbKey,
        xfbml: true,
        version: 'v2.1'
    });
};

(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));


class FacebookService {

    constructor($q, $route, $window, ConfigService) {
        this.$q = $q;
        this.$route = $route;
        this.$window = $window;

        this._userId = null;
        this._accessToken = null;
        this._isConnected = false;

        this.showProfilePicture = ConfigService.checkShowProfilePicture();
        this.showQuestionPicture = ConfigService.checkShowQuestionPicture();
    }

    getPostLight(socialPost) {
        let url = encodeURIComponent(Config.shareUrl);
        let redirect = encodeURIComponent(Config.webUrl);
        if (this.$route) {
            let routeParam = this.$route.current.$$route.name;
            redirect = url + encodeURIComponent("/#/" + routeParam + "?");
        }

        let fbPost = "https://www.facebook.com/dialog/feed?";
        fbPost += "app_id=" + Config.fbKey;
        fbPost += "&display=popup";
        fbPost += "&caption=" + url;
        fbPost += "&name=" + socialPost.message.message;
        if (socialPost.message.description) fbPost += "&description=" + socialPost.message.description;
        fbPost += "&link=" + Config.shareUrl;
        if (this.showProfilePicture) fbPost += "&picture=" + encodeURIComponent(socialPost.message.picture);
        fbPost += "&redirect_uri=" + redirect;
        return fbPost;
    };

    publishPostLight(message) {
        this.$window.open(message, "_blank");
    };

    checkLoginStatus() {
        var deferred = this.$q.defer();

        FB.getLoginStatus((response) => {
//console.log('check login status = ' + response.status);

            if (response.status === 'connected') {
                this.setAuth(response);

                this.getUserData().then((me) => {
                    deferred.resolve(me);
                });
            } else if (response.status === 'not_authorized') {
                deferred.resolve(null);
            } else { // the user isn't logged in to Facebook.
                deferred.resolve(null);
            }
        });

        return deferred.promise;
    };

    setAuth(response) {
        if (response.authResponse) {
            this._userId = response.authResponse.userID;
            this._accessToken = response.authResponse.accessToken;
        }
    };

    /**
     * Login methods
     *
     */
    goLogin() {
        var deferred = this.$q.defer();

//console.log("go login with facebook");
        FB.login((response) => {
//console.log('facebook login response = ', response);
            if (response.status === 'connected') {
                this.setAuth(response);

                this.getUserData().then(function (me) {
                    deferred.resolve(me);
                });
            }
            else if (response.status == "not_authorized") {
                deferred.resolve(null);
            } else {
                deferred.reject();
            }
        }, {scope: 'user_about_me, publish_actions'});

        return deferred.promise;
    };

    getUserData() {
        var deferred = this.$q.defer();

        FB.api('/me', (response) => {
            this._isConnected = true;

            var username = (response.name != undefined) ? response.name : this._userId;
            var me = {};
            me.userId = this._userId;
            me.token = this._accessToken;
            me.name = username;
            me.first_name = response.first_name;
            me.last_name = response.last_name;
            me.gender = response.gender;
            me.thumb = "http://graph.facebook.com/" + this._userId + "/picture?type=square";
            deferred.resolve(me);
        });

        return deferred.promise;
    };

    getAnswerPost(user, answer) {
        var post = {};
        post.platform = "facebook";
        post.user = user;

        var message = this.getDefaultMessage();
        if (answer) {
            message.message = answer.text;
            message.name = "";
            if (this.showQuestionPicture) message.picture = Config.webUrl + Config.imageUrl + "/backgrounds/1200/statement-" + answer.questionId + ".jpg";
        }

        post.message = message;
        // post.title = profile.titleText;
        return post;
    };

    getProfilePost(user, profile) {
        var post = {};
        post.platform = "facebook";
        post.user = user;

        var message = this.getDefaultMessage();
        if (profile) {
            message.message = profile.shareText;
            message.name = "";
            if (this.showProfilePicture) message.picture = Config.webUrl + Config.profileUrl + profile.label + ".jpg";
            message.description = profile.teaserText;
        }

        post.message = message;
        post.title = profile.titleText;
        return post;
    };

    getDefaultMessage() {
        var message = {
            display: 'iframe',
            actions: [{name: 'action_links text!', link: Config.shareUrl}],
            caption: Config.shareUrl,
            link: Config.shareUrl  // Go here if user click the picture
        };
        return message;
    };

    publishPost(message) {
        var deferred = this.$q.defer();

        FB.api("/me/feed", "POST", message, (response) => {
                if (response && !response.error) {
                    deferred.resolve(response);
                }
                else {
                    var message = response.error.message;
                    if (message == "The user hasn't authorized the application to perform this action") {
                        this.goLogin();
                        deferred.reject();
                    } else {
                        deferred.reject();
                    }
                }
            }
        );

        return deferred.promise;
    };

    static FacebookFactory($q, $route, $window, ConfigService) {
        return new FacebookService($q, $route, $window, ConfigService);
    }
}

FacebookService.FacebookFactory.$inject = ['$q', '$route', '$window', 'ConfigService'];

angular.module('coen.services.facebook', [])
    .factory('FacebookService', FacebookService.FacebookFactory);

export default FacebookService;