import angular from 'angular';

import answerService from './answer/answer.service';
import configService from './config/config.service';
import dataService from './data/data.service';
import emailService from './email/email.service';
import facebookService from './facebook/facebook.service';
import highchartService from './highchart/highchart.service';
import menuService from './menu/menu.service';
import restService from './rest/rest.service';
import twitterService from './twitter/twitter.service';

const vabServicesModule = angular.module('coen.services', [
    'coen.services.answer',
    'coen.services.config',
    'coen.services.data',
    'coen.services.email',
    'coen.services.facebook',
    'coen.services.highchart',
    'coen.services.menu',
    'coen.services.rest',
    'coen.services.twitter'
]);

export default vabServicesModule;
