import angular from 'angular';

import template from './info.html';
import controller from './info.controller';
import style from './_info.scss';

import formComponent from './form-component/form-component.component';

var infoModule = angular.module('coen.features.info', [
    formComponent.name
]);

infoModule.component('infoFeature', {
    template: template,
    controller: controller
});

export default infoModule;
