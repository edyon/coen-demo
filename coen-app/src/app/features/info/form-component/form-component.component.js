import angular from 'angular';

import controller from './form-component.controller';
import formCoen from './form-coen.template.html';
import formHardenberg from './form-hardenberg.template.html';
import formRoc from './form-roc.template.html';
import formSpov from './form-spov.template.html';
import formVerkiezingen from './form-verkiezingen.template.html';

var formModule = angular.module('coen.features.form.coen', []);

formModule.component('formCoen', {
    template: formCoen,
    controller: controller,
    bindings: {
        user: '='
    }
});

formModule.component('formHardenberg', {
    template: formHardenberg,
    controller: controller,
    bindings: {
        user: '='
    }
});

formModule.component('formRoc', {
    template: formRoc,
    controller: controller,
    bindings: {
        user: '='
    }
});

formModule.component('formSpov', {
    template: formSpov,
    controller: controller,
    bindings: {
        user: '='
    }
});

formModule.component('formVerkiezingen', {
    template: formVerkiezingen,
    controller: controller,
    bindings: {
        user: '='
    }
});

export default formModule;
