import angular from 'angular';

import template from './opinion.html';
import controller from './opinion.controller';

var opinionModule = angular.module('coen.features.opinion', []);

opinionModule.component('opinionFeature', {
    template: template,
    controller: controller
});

export default opinionModule;
