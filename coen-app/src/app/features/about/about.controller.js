export default class AboutController {

    constructor($rootScope, MenuService, $filter) {
        this.$filter = $filter;

        this.user = null;

        this.questions = MenuService.getQuestionsByMenu("about");

        if (this.questions[0] && $filter('translate')(this.questions[0].question) != this.questions[0].question) {
            this.setQuestions();
        } else {
            $rootScope.$on('$translateChangeSuccess', ()=> {
                this.setQuestions();
            });
        }
    }

    setQuestions() {
        if (this.questions[0] && this.$filter('translate')(this.questions[0].question) != this.questions[0].question) this.question1 = this.questions[0];
        if (this.questions[1] && this.$filter('translate')(this.questions[1].question) != this.questions[1].question) this.question2 = this.questions[1];
        if (this.questions[2] && this.$filter('translate')(this.questions[2].question) != this.questions[2].question) this.question3 = this.questions[2];
        if (this.questions[3] && this.$filter('translate')(this.questions[3].question) != this.questions[3].question) this.question4 = this.questions[3];
        if (this.questions[4] && this.$filter('translate')(this.questions[4].question) != this.questions[4].question) this.question5 = this.questions[4];
        if (this.questions[5] && this.$filter('translate')(this.questions[5].question) != this.questions[5].question) this.question6 = this.questions[5];
    }
}

