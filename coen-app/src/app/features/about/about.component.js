import angular from 'angular';

import template from './about.html';
import controller from './about.controller';
import style from './_about.scss';

var aboutModule = angular.module('coen.features.about', []);

aboutModule.component('aboutFeature', {
    template: template,
    controller: controller
});

export default aboutModule;
