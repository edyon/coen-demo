export default class ReminderController {

    constructor($scope, checkUser, MenuService, RestService) {
        this.$scope = $scope;

        this.RestService = RestService;

        this.user = checkUser;

        $scope.$on('save-reminder', () => {
            this.savePerson();
        });


        this.questions = MenuService.getQuestionsByMenu("reminder");
        this.question1 = this.questions[0];
        this.question2 = this.questions[1];
        this.question3 = this.questions[2];
        this.question4 = this.questions[3];
    }

    savePerson  () {
        this.RestService.savePerson(this.user).then(() => {
            // save person complete
        });
    };
}
