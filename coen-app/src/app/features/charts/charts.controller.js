export default class ChartsController {

    constructor($scope, MenuService) {
        this.$scope = $scope;

        this.MenuService = MenuService;

        this.results = [];
        this.allResults = [];
        this.likedVotes = [];

        this.questions = this.MenuService.getQuestionsByMenu("resultat");
        this.question1 = this.questions[0];

        this.getResults();
    }

    getResults() {

        this.RestService.getCompleteSurvey().then((survey) => {
//console.log("complete survey = ",  survey);
            if (survey) {
                this.RestService.getThemeScore(survey.id).then((scores) => {
                    this.setResult(scores);
                });
            }
        }).catch((error) => { // handle 401 error
            this.RestService.getThemeScore(0).then((scores) => {
                this.setResult(scores);
            });
        });
    };

    setResult(scores) {
        var allResults = scores;
//console.log("average score results = ", allResults);
        if (allResults) {
            _.sortBy(allResults, function (result) {
                return result.agreeCount;
            });

            this.allResults = allResults;
        }
    }
}