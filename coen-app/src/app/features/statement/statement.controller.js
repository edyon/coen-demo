export default class StatementController {

    constructor($scope, $rootScope, $routeParams, $location, $timeout, DataService, MenuService, AnswerService) {
        this.$scope = $scope;
        this.$routeParams = $routeParams;
        this.$rootScope = $rootScope;
        this.$location = $location;
        this.$timeout = $timeout;

        this.DataService = DataService;
        this.MenuService = MenuService;
        this.AnswerService = AnswerService;

        this.answers = null;
        this.answerA = null;
        this.answerB = null;
        this.currentAnswer = null;
        this.currentIndex = 0;
        this.currentPane = 0;
        this.testSelected = false;

        this.getData();

        /**
         * watch if open question is answered
         */
        $scope.$watch('$ctrl.currentAnswer.submittedAnswer', () => {
            if (this.currentAnswer && this.currentAnswer.type == "open") {
                if (this.currentAnswer.submittedAnswer && this.currentAnswer.submittedAnswer != "") {
                    $rootScope.$broadcast('question:answered');
                }
            }
        });

        /**
         * handle saving open question
         */
        $scope.$on('question:submit', (event, args) => {
            AnswerService.saveOpenAnswer(this.currentAnswer.submittedAnswer);
        });

        /**
         * reset after swiping when we show correct answer
         */
        $scope.$on('question:reset', (event, args) => {
            this.$timeout(() => {
                this.selectCurrentAnswer(this.currentIndex);
            });
        });
    }

    getData() {
        this.DataService.checkUser().then((user) => {
            this.user = user;

            this.DataService.checkSurvey().then(survey => {
                this.survey = survey;
                this.setAnswerFromRoute();
            });
        });
    }

    /**
     * select the answer from route
     */
    setAnswerFromRoute() {
        this.answers = this.survey.answers;
        this.currentIndex = 0;

        let statementId = this.$routeParams.statementId;
//console.log("set answer from route = ", statementId, " survey = ", this.survey);
        if (statementId != undefined) {
            if (!this.currentAnswer) {
                for (var i = 0; i < this.answers.length; i++) {
                    var answer = this.answers[i];
                    if (answer.questionId == statementId) {
                        this.currentIndex = i;
                        break;
                    }
                }
                this.selectCurrentAnswer(this.currentIndex);
            }
        } else {
            for (var i = 0; i < this.answers.length; i++) {
                if (!this.answers[i].correct) {
                    this.currentIndex = i;
                    break;
                }
            }
//$log.log("statement index found = ", $scope.currentIndex, " --- answers = ",  $scope.answers.length);
            let currentAnswer = this.answers[this.currentIndex];
            this.navigatePath("statement/" + currentAnswer.questionId);
        }
    };

    navigatePath(url) {
        this.$timeout(() => {
            this.$location.path(url);
        }, 100);
    };

    /**
     * handle selecting current answer
     *
     * @param index
     */
    selectCurrentAnswer(index) {
        this.currentIndex = index;
        if (!this.checkSurveyComplete()) {
            this.nextAnswer = this.answers[index + 1];
            this.answerA = this.answers[index];
            this.currentAnswer = this.answerA;
            this.answerB = this.nextAnswer;
            this.resetPane(this.currentAnswer);
            this.currentPane = 0;

// console.log(">>> select current answer = ", this.currentAnswer, " index = ", this.currentIndex);
            if (this.currentAnswer.correct != null) {
                this.$rootScope.$broadcast('question:answered');
            }

            this.DataService.setAnswer(this.currentAnswer, this.currentIndex);

            this.AnswerService.setSurvey(this.survey);
            this.AnswerService.setCurrentQuestion(this.currentAnswer);
        }
    };

    resetPane(current) {
        $('#answerA').css('transform', 'translate(0px, 0px)');
        $('#answerA').css('display', 'block');
        $('#answerB').css('transform', 'translate(0px, 0px)');
        $('#answerB').css('display', 'block');

        if (this.answerA && current == this.answerA) {
            $('#answerB').css('z-index', 0);
            $('#answerA').css('z-index', 1);
        } else if (this.answerB && current == this.answerB) {
            $('#answerA').css('z-index', 0);
            $('#answerB').css('z-index', 1);
        }
    };

    /**
     * handle animating the current answer
     *
     * @param index
     */
    animateCurrentAnswer(index) {
        this.currentIndex = index;

        if (!this.checkSurveyComplete()) {
            if (this.answers) {
                if (index < this.answers.length) {
                    this.nextAnswer = this.answers[index + 1];
                    if (!this.currentAnswer) { // initial situation
                        this.answerA = this.answers[0];
                        this.currentAnswer = this.answerA;
                        this.answerB = this.nextAnswer;
                        this.currentPane = 0;
                    }
                    else if (this.currentAnswer == this.answerA) {
                        this.answerA = this.nextAnswer;
                        this.currentAnswer = this.answerB;
                        this.currentPane = 1;
                    }
                    else {
                        this.answerB = this.nextAnswer;
                        this.currentAnswer = this.answerA;
                        this.currentPane = 0;
                    }
                }
// console.log(">>> animate current answer = ", this.currentAnswer, " index = ", this.currentIndex);
                if (this.currentAnswer.correct != null) {
                    this.$rootScope.$broadcast('question:answered');
                }

                this.DataService.setAnswer(this.currentAnswer, this.currentIndex, this.currentPane);

                this.AnswerService.setSurvey(this.survey);
                this.AnswerService.setCurrentQuestion(this.currentAnswer);
            }
        }
    };


    /**
     * go to the next answer
     */
    goToNextAnswer(animate) {
        if (this.currentIndex <= this.answers.length) {
            this.currentIndex++;
        }

        if (animate === true) {
            this.animateCurrentAnswer(this.currentIndex);
        } else {
            this.selectCurrentAnswer(this.currentIndex);
        }
    };

    voteStatement(animate) {
        this.goToNextAnswer(animate);

        var current = this.currentAnswer;
        if (!this.checkSurveyComplete) {
            this.$timeout(() => {
                this.$location.path('statement/' + current.questionId);
            }, 100);

        }
    };

    checkSurveyComplete() {
        if (this.currentIndex === this.answers.length) { // test complete
            var nextMenu = this.MenuService.getAfterQuestionnaireMenu();
            this.$location.path(nextMenu.url);
            return true;
        }
        return false
    };
}
