export default class ResultController {

    constructor($scope, $filter, $sce, RestService, DataService, MenuService) {
        this.$scope = $scope;
        this.$filter = $filter;
        this.$sce = $sce;

        this.RestService = RestService;
        this.DataService = DataService;
        this.MenuService = MenuService;

        this.results = [];
        this.allResults = [];
        this.likedVotes = [];

        this.menus = MenuService.getMenu();
        this.menu = this.menus[this.menus.length - 1];

        this.getResults();
    }

    getResults () {
        this.RestService.getCompleteSurvey().then((survey) => {
            this.survey = survey;
            if (this.survey) {
                this.setUserProfile();
            }
        });
    };

    setUserProfile () {
        let correctAnswers = _.filter(this.survey.answers, (answer) => {
            return answer.correct === true;
        });

        this.numQuestions = this.survey.answers.length;
        this.numCorrectAnswers = correctAnswers.length;
        this.profileScore = this.MenuService.getProfileScore(this.numCorrectAnswers, this.numQuestions);

        this.getAllResults();
    };

    getAllResults  () {
        this.scores = [];

        // TODO get results for this campaign only
        this.RestService.getResults().then((scores) => {
            let scoreObject = this.getScoreObject(scores);

            _.each(scoreObject, (questions) => {
                if (questions && questions.length > 0) {
                    let firstQuestion = questions[0];
                    let correctQuestions = _.filter(questions, (question) => {
                        if (!question.answer) {
                            return question.correct == true;
                        } else {
                            return question.answer == 0; // statement agree is 0
                        }
                    });

                    let agreeCount = correctQuestions ? correctQuestions.length : 0;
                    let disagreeCount = correctQuestions ? (questions.length - correctQuestions.length) : 0;
                    let agreePercentage = (agreeCount / questions.length) * 94;
                    let disagreePercentage = (disagreeCount / questions.length) * 94;

                    if (firstQuestion.type == "multiple" ) {
                        var answerObject = {};
                        _.each(firstQuestion.choices, (choice, index) => {
                            let answerQuestions = _.filter(questions, (question) => {
                                return question.answer == index;
                            });
                            answerObject[index] = {
                                answerCount: answerQuestions.length,
                                answerPercentage: (answerQuestions.length / questions.length) * 94
                            };
                        });
                    }


                    let scoreObj = {
                        question: firstQuestion,
                        questions: questions,
                        userCount: questions.length,
                        agreeCount: agreeCount,
                        disagreeCount: disagreeCount,
                        agreePercentage: agreePercentage,
                        disagreePercentage: disagreePercentage,
                        answerObject: answerObject
                    };

                    this.scores.push(scoreObj);
                }
            });
        });
    };

    getScoreObject(scores) {
        let scoreObject = {};
        _.each(scores, (score) => {
            if (score.campaignId == this.survey.campaignId) {
                _.each(score.answers, (answer) => {
                    let questions = scoreObject[answer.questionId] ? scoreObject[answer.questionId] : [];
                    questions.push(answer);
                    scoreObject[answer.questionId] = questions;
                });
            }
        });
        return scoreObject;
    }

    setAllProfiles () {
        this.profile = this.MenuService.getProfileByScore(this.profileScore, this.profiles);
        this.MenuService.setShareText(this.profile, this.numQuestions, this.numCorrectAnswers);

        this.DataService.setProfile(this.profile);

        let profiles = this.MenuService.getProfiles();
        this.profiles = this.MenuService.getProfilesWithScore(profiles, this.userCount, this.scores);
// console.log("set all person profiles = ", this.profiles, " out of all persons = ", this.scores.length);
    };


    trustAsHtml (html) {
        var translated = this.$filter('translate')(html);
        return this.$sce.trustAsHtml(translated);
    };
}
