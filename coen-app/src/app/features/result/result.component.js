import angular from 'angular';

import template from './result.html';
import controller from './result.controller';
import style from './_result.scss';

var resultModule = angular.module('coen.features.result', []);

resultModule.component('resultFeature', {
    template: template,
    controller: controller
});

export default resultModule;
