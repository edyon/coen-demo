import angular from 'angular';

import template from './popup.html';
import style from './popup.scss';

var popupModule = angular.module('coen.features.popup', []);

popupModule.component('popupFeature', {
    template: template
});

export default popupModule;
