import angular from 'angular';

import template from './social.html';
import controller from './social.controller';
import style from './_social.scss';

var socialModule = angular.module('coen.features.social', []);

socialModule.component('socialFeature', {
    template: template,
    controller: controller
});

export default socialModule;
