import angular from 'angular';

import template from './home.html';
import controller from './home.controller';
import style from './_home.scss';

var homeModule = angular.module('coen.features.home', []);

homeModule.component('homeFeature', {
    template: template,
    controller: controller
});

export default homeModule;
