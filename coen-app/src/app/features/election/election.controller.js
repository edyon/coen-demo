export default class ElectionController {

    constructor($scope, $location, checkUser, RestService) {
        this.$scope = $scope;
        this.$location = $location;

        this.RestService = RestService;

        this.user = checkUser;

        $scope.$on('election:agree', (event, args) => {
            this.user.fulfillsDuty = true;
            this.saveElectionUser();
        });

        $scope.$on('election:disagree', (event, args) => {
            this.user.fulfillsDuty = false;
            this.saveElectionUser();
        });
    }

    saveElectionUser () {
        this.RestService.savePerson(this.user).then(() => {
            this.$location.path("overzicht");
        }).catch((error) => {
            this.$location.path("overzicht");
        });
    };
}