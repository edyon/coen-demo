export default class OverviewController {

    constructor($scope, $location, RestService, HighchartService, MenuService, DataService) {
        this.$scope = $scope;
        this.$location = $location;

        this.RestService = RestService;
        this.HighchartService = HighchartService;

        DataService.checkUser().then(user => {
            this.user = user;

            this.survey = null;
            this.results = [];
            this.allResults = [];
            this.likedVotes = [];

            this.questions = MenuService.getQuestionsByMenu("overview");
            this.question1 = this.questions[0];

            this.themeInfo = {name: ''};
            this.showTheInfo = true;

            this.numImportant = 0;
            this.getResults();
        });

        $scope.$on('chart:info', (event, args) => {
            this.selectThemeInfo(args.category);
        });
    }

    getResults() {
        this.showTheInfo = false;

        this.RestService.getCompleteSurvey().then(survey => {
            this.survey = survey;
            this.setScore();

            if (this.survey) {
                this.getThemeScore();
            }
        }).catch(error => {
            this.handleError();
        });
    }

    handleError() {
        this.$location.path("home");
    }

    setScore() {
        this.likedVotes = _.filter(this.survey.answers, (vote) => {
            return vote.agree === true;
        });

        this.numImportant = 0;
        _.each(this.survey.answers, (vote) => {
            if (vote.agree === true) {
                if (vote.important === true) {
                    this.numImportant++;
                }
            }
        });
    }

    getThemeScore() {
        let dataAgree;
        let dataDisagree;

        this.RestService.getResults(this.survey.id).then(scores => {
            var allResults = scores;

            if (allResults) {
                let personalResult = _.find(allResults, (result) => {
                    return result.personId == this.user.id;
                });

                this.allResults = personalResult.answers;
                dataAgree = this.HighchartService.convertToScore(allResults, true);
                dataDisagree = this.HighchartService.convertToScore(allResults, false);
                this.chartConfigAll = this.HighchartService.getChartConfig(dataAgree, dataDisagree, false, "column");
                this.chartConfigAllMobile = this.HighchartService.getChartConfig(dataAgree, dataDisagree, false, "bar");
// console.log("chartConfigAll = ", this.allResults, personalResult, this.chartConfigAll);
            }
        });
    };

    selectThemeInfo(name) {
        //var info = '<p>Je geeft aan dat {{ theme.name }} voor jou een belangrijk onderwerp is. Naast de onderwerpen die je voorbij hebt zien komen spelen er natuurlijk nog meet zaken rondom onderwijs. Wil je weten welke partijen onderwijs ook erg belangrijk vinden? Vul de stemwijzer in en ontdek welke partijen het beste bij jou passen.</p>';
        let info = "test";

        if (this.themeInfo.name != name) {
            let scoreInfo = _.find(this.results, (score) => {
                return score.themeDto.name === name;
            });

            this.themeInfo = {
                name: scoreInfo.themeDto.name,
                description: info
            };

            this.showTheInfo = true;
//console.log("open theme info in controller = ", this.themeInfo, " - show me now = ", this.showTheInfo);
        }
    };


    toggleVoteImportant(vote) {
        if (vote.important) {
            if (this.numImportant > 0) {
                this.numImportant--;
                this.toggleVote(vote);
            }
        } else {
            if (this.numImportant < 3) {
                this.numImportant++;
                this.toggleVote(vote);
            }
        }
    };

    toggleVote(vote) {
        vote.important = vote.important != null ? !vote.important : true;

        this.RestService.answerQuestion(vote).then(vote => {
            // save vote complete
        });
    };
}
