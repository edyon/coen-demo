import angular from 'angular';

import template from './overview.html';
import controller from './overview.controller';
import style from './overview.scss';

var overviewModule = angular.module('coen.features.overview', []);

overviewModule.component('overviewFeature', {
    template: template,
    controller: controller
});

export default overviewModule;
