import angular from 'angular';

import template from './total.html';
import controller from './total.controller';

var totalModule = angular.module('coen.features.total', []);

totalModule.component('totalFeature', {
    template: template,
    controller: controller
});

export default totalModule;
