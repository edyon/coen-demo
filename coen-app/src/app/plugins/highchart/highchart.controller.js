export default class highchartController {

    constructor($scope, $rootScope, $element, $route, $window, $document, HighchartService) {
        this.$scope = $scope;
        this.$rootScope = $rootScope;
        this.$route = $route;
        this.$window = $window;
        this.$element = $element;
        this.$document = $document;

        this.HighchartService = HighchartService;

        this.chart = null;
        this.currentType = null;
        this.viewWidth = 0;

        $(window).resize(() => {
            this.initChart();
        });

        $scope.$on('chart:animated', (event, data) => {
            this.setChartCssProps();
        });

        this.initChart();

        $scope.$watch("$ctrl.config.series", (newSeries, oldSeries) => {
            if (this.chart) {
                this.HighchartService.processSeries(this.chart, newSeries);
                this.chart.redraw();
            }
        });

        $scope.$watch("$ctrl.config.options", (newOptions, oldOptions, scope) => {
            this.initChart();
        });

        $scope.$on('chart:complete', () => {
            $('.btn-info-chart').bind('click', () => {
                this.handleInfoClick();
            });
        });

        $scope.$on('chart:animated', () => {
            $('.btn-info-chart').bind('click', () => {
                this.handleInfoClick();
            });
        })
    }


    /**
     * set highchart theme
     */
    initChart() {
        if (this.config) {
            var barType = this.getTypeForScreen();
            if (this.currentType != barType && this.barType === barType) {
//console.log("init chart = ", barType, " == ", scope.barType, " config = ", scope.config);

                this.currentType = barType;
                if (this.chart) {
                    this.chart.destroy();
                }

                let theme = this.HighchartService.getChartTheme(this.barType);

                // TODO is this defined?
                Highcharts.theme = theme;
                Highcharts.setOptions(theme);

                var containerId = attrs.id;
                this.config.chart = {};
                this.config.chart.renderTo = containerId;
                this.config.chart.type = this.barType;
                this.chart = this.HighchartService.initialiseChart(this, this.$element, this.config, theme);
            }
        }
    }

    getTypeForScreen() {
        let barType;
        this.viewWidth = this.$document.documentElement.clientWidth;
        if (this.viewWidth > 0) {
            barType = this.viewWidth >= 768 ? "column" : "bar";
        }
        return barType;
    }

    setChartCssProps() {
        this.viewWidth = $document.documentElement.clientWidth;
        $('.chart-info.bar').css('width', this.viewWidth);
    };

    handleInfoClick(event) {
// console.log("click info button id = ", event.currentTarget.id);
        this.$rootScope.$broadcast('chart:info', {category: event.currentTarget.id});
    };

}