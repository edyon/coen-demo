import angular from 'angular';

import template from './fonts.template.html';
import controller from './fonts.controller';

import coenFonts from './coen-font-library/coen-font-library.component';
import hardenbergFonts from './hardenberg-font-library/hardenberg-font-library.component';
import rocFonts from './roc-font-library/roc-font-library.component';

const FontsModule = angular.module('coen.components.plugins.fonts', [
    coenFonts.name,
    hardenbergFonts.name,
    rocFonts.name
]);

FontsModule.component('fontLibrary', {
    template: template,
    controller: controller
});

export default FontsModule;
