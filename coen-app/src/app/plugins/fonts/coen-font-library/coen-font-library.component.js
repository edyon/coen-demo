import angular from 'angular';

import template from './coen-font-library.html';

var coenFontModule = angular.module('coen.components.fonts.coen', []);

coenFontModule.component('coenFontLibrary', {
    template: template
});

export default coenFontModule;
