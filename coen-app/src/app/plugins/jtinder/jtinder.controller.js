export default class TinderController {

    constructor($scope, $element, AnswerService) {
        this.$scope = $scope;
        this.$element = $element;

        this.AnswerService = AnswerService;

        if (!this.divTinder) {
            this.divTinder = $($element).jTinder({
                onDislike: () => {
                    this.onDislike();
                },
                onLike: () => {
                    this.onLike();
                },
                animationRevertSpeed: this.animationRevertSpeed,
                animationSpeed: this.animationSpeed,
                threshold: this.threshold,
                likeSelector: '.like',
                dislikeSelector: '.dislike'
            });
        }

        /**
         * Set button action to trigger jTinder like & dislike.
         */
        $($element).find('.btn_like, .btn_dislike').bind("click", (e) => {
            e.preventDefault();
            var divId = e.currentTarget.id;
            if (divId === "btn_like") {
                this.divTinder.jTinder('like');
            } else if (divId === "btn_dislike") {
                this.divTinder.jTinder('dislike');
            }
        });

        $scope.$watch('$ctrl.animateLike', () => {
            if (this.animateLike === true) {
                this.divTinder.jTinder('like');
            }
        });

        $scope.$watch('$ctrl.animateDislike', () => {
            if (this.animateDislike === true) {
                this.divTinder.jTinder('dislike');
            }
        });

        // handle navigate from statement
        $scope.$on('next-statement', (event, args) => {
            var pane = args.currentPane;
            if (pane == 0) {
                this.divTinder.jTinder('setFirstPane');
            } else {
                this.divTinder.jTinder('setLastPane');
            }
        });
    }

    onLike() {
        this.AnswerService.saveAnswer(0, true, true);
    }

    onDislike() {
        this.AnswerService.saveAnswer(1, true, true);
    }
}