export default class SocialPostController {

    constructor($scope, FacebookService, TwitterService, RestService) {
        this.$scope = this;

        this.FacebookService = FacebookService;
        this.TwitterService = TwitterService;
        this.RestService = RestService;

        this.showInfo = false;

        $scope.$on('toggle:social-post', (event, data) => {
            this.post = data.post;
            this.showInfo = true;
//console.log("toggle post = ", $scope.post, $scope.showInfo);
        });
    }

    sharePost() {

        if (this.post.platform == "facebook") {
            this.FacebookService.publishPost(this.post.message).then((response) => {
                if (response.id) {
                    this.close();
                }
            });
        } else if (this.post.platform == "twitter") {
            this.TwitterService.publishPost(this.post.message).then((response) => {
                if (response.id) {
                    this.close();
                }
            });
        } else if (this.post.platform == "email") {
            var email = {
                "emailAdress": this.post.email,
                "subject": this.post.message.name,
                "valueMap": {
                    "imgUrl": this.post.message.picture,
                    "body": this.post.message.name + "</br>" + this.post.message.description
                }
            };

            this.RestService.sendEmail(email).then((response) => {
                this.close();
            });
        }
    };

    close() {
        this.showInfo = false;
    };
}