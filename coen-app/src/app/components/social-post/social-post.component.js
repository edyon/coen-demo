import angular from 'angular';

import template from './social-post.template.html';
import controller from './social-post.controller';

var socialPostModule = angular.module('coen.components.social.post', []);

socialPostModule.component('socialPost', {
    template: template,
    controller: controller,
    bindings: {
        info: '=',
        type: '@type'
    }
});

export default socialPostModule;
