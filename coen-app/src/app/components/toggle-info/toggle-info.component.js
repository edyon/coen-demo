import angular from 'angular';

import template from './toggle-info.template.html';
import controller from './toggle-info.controller';
import style from './_toggle-info.scss';

var infoModule = angular.module('coen.components.toggle-info', []);

infoModule.component('toggleInfo', {
    template: template,
    controller: controller,
    bindings: {
        info: '=',
        type: '@type'
    }
});

export default infoModule;
