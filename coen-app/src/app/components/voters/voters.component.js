import angular from 'angular';

import template from './voters.template.html';
import controller from './voters.controller';

var votersModule = angular.module('coen.components.voters', []);

votersModule.component('coolVoters', {
    template: template,
    controller: controller,
    bindings: {
        active: '='
    }
});

export default votersModule;
