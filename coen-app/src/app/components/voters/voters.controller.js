export default class VotersController {

    constructor($scope, $location, $route, RestService, DataService) {
        this.$scope = $scope;
        this.$location = $location;
        this.$route = $route;

        this.RestService = RestService;
        this.DataService = DataService;

        this.user = DataService.checkUser();
        this.votePercentage = 100;
        this.getVoters();
    }

    getVoters () {
        this.RestService.getPeopleCount().then((scores) => {
            this.numberOfScores = scores.count;
            this.numberOfCoolPeople = scores.count;

            this.RestService.getCoolCount().then((voters) => {
//console.log("show voters = ", this.active);
                if (this.active === true) {
                    this.numberOfCoolPeople = voters.count;
//console.log("voters found = ", this.numberOfCoolPeople, " --- scores found = ", this.numberOfScores);

                    var score = (this.numberOfCoolPeople / this.numberOfScores) * 100;
                    this.votePercentage = score;
                }
            });
        });
    }

    showRight () {
        return this.numberOfCoolPeople == this.numberOfScores;
    }
}