import angular from 'angular';

import template from './theme.template.html';
import controller from './theme.controller';

var themeModule = angular.module('coen.components.theme', []);

themeModule.component('themeView', {
    template: template,
    controller: controller,
    bindings: {
        themeRating: '=',
        favourite: '='
    }
});

export default themeModule;
