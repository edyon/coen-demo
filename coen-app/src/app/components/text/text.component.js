import angular from 'angular';

import controller from './text.controller';

var textFormatModule = angular.module('coen.components.text.format', []);

textFormatModule.component('textFormat', {
    template: `<div class="text" ng-bind-html="$ctrl.parsedText"></div>`,
    controller: controller,
    bindings: {
        text: '='
    }
});

export default textFormatModule;
