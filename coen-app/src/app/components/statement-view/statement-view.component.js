import angular from 'angular';

import template from './statement-view.template.html';
import controller from './statement-view.controller';
import style from './_statement-view.scss';

var statementModule = angular.module('coen.components.statement', []);

statementModule.component('statementView', {
    template: template,
    controller: controller,
    bindings: {
        answer: '='
    }
});

export default statementModule;
