export default class StatementController {

    constructor($scope, $rootScope, AnswerService, ConfigService) {
        this.$scope = $scope;
        this.$rootScope = $rootScope;

        this.AnswerService = AnswerService;
        this.ConfigService = ConfigService;

        this.info = {};
        this.showInfo = false;

        $scope.$watch('$ctrl.answer', () => {
            this.currentAnswer = null;
            this.setCorrectInfo();
        });

        $scope.$watch('$ctrl.answer.correct', () => {
            this.setCorrectInfo();
        });

        $scope.$on("close-info-popup", () => {
            this.showInfo = false;
        });

        // handle navigate from statement
        $scope.$on('next-statement', (event, args) => {
            //$rootScope.$broadcast('toggle:popup');
        });
    }

    setCorrectInfo() {
        if (this.answer) {
            this.info.description = this.answer.info;
            this.info.correct = this.answer.correct;

            if (this.ConfigService.checkShowCorrectInfo()) {
                var isAnswered = this.answer.answer || this.answer.correct;
                this.showInfo = isAnswered && this.answer.info;
            }
        }
    };

    getInfo() {
        this.showInfo = !this.showInfo;
    };

    /**
     * handle clicking a multiple choice question
     */
    selectAnswer(statement, index) {
        this.currentAnswer = index;
        this.AnswerService.saveAnswer(index, true, false);
    };

    parseImageUrl(imageId) {
        var fileType = Config.client == "spov" ? ".png" : ".jpg";
        var imageUrl = imageId ? "question-" + imageId + fileType : "";
        return imageUrl;
    };
}