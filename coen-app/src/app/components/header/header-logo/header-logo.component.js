import angular from 'angular';

import template from './header-logo.template.html';

var headerLogoModule = angular.module('coen.components.header.logo', []);

class HeaderLogoController {

    constructor($scope, $location) {
        this.$location = $location;

        $scope.$on('menu:select', (event, args) => {
            this.menu = args.menu;
        });
    }

    goHome(item) {
        this.$location.path("/home");
    };
}

headerLogoModule.component('headerLogo', {
    template: template,
    controller: HeaderLogoController
});

export default headerLogoModule;