import angular from 'angular';

import template from './header.template.html';
import style from './_header.scss';

import bulletBar from './bullet-bar/bullet-bar.component';
import burgerMenu from './burger/burger.component';
import headerLogo from './header-logo/header-logo.component';
import headerLogo2 from './header-logo/header-logo2.component';

var headerModule = angular.module('coen.components.header', [
    bulletBar.name,
    burgerMenu.name,
    headerLogo.name,
    headerLogo2.name
]);

headerModule.component('headerComponent', {
    template: template
});

export default headerModule;
