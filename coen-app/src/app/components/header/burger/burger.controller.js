export default class appController {

    constructor($scope, $rootScope, MenuService) {
        this.$rootScope = $rootScope;

        this.items = MenuService.getBurgerItems();

        $scope.$on('menu:select', (event, args) => {
            this.menu = args.menu;
        });
    }

    toggleMenu () {
        this.$rootScope.$broadcast('toggle:popup');
    }

    select (item) {
        this.$rootScope.$broadcast('toggle:popup', {type: 'burger', info: item});
    };
}
