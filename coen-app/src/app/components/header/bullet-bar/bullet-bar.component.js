import angular from 'angular';

import template from './bullet-bar.template.html';
import controller from './bullet-bar.controller';
import style from './_bullet-bar.scss';

var bulletbarModule = angular.module('coen.components.bulletbar', []);

bulletbarModule.component('bulletBar', {
    template: template,
    controller: controller
});

export default bulletbarModule;
