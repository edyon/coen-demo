import angular from 'angular';

import controller from './thumb.controller';

var thumbModule = angular.module('coen.components.thumb', []);

thumbModule.component('imageThumb', {
    template: `<img id='image' alt='{{ $ctrl.altText }}' title='{{ $ctrl.title }}'/>`,
    controller: controller,
    bindings: {
        imageUrl: '=',
        imageId: '=',
        altText: '@?',
        title: '@?'
    }
});

export default thumbModule;
