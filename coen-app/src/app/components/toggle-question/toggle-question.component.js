import angular from 'angular';

import template from './toggle-question.template.html';
import controller from './toggle-question.controller';
import style from './_toggle-question.scss';

var questionModule = angular.module('coen.components.toggle-question', []);

questionModule.component('toggleQuestion', {
    template: template,
    controller: controller,
    bindings: {
        question: '=',
        buttonClass: '@',
        important: '=',
        fieldValue: '='
    }
});

export default questionModule;
