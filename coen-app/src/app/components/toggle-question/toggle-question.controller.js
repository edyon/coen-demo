export default class appController {

    constructor($scope, $rootScope, $location, $sce) {
        this.$scope = $scope;
        this.$rootScope = $rootScope;
        this.$location = $location;
        this.$sce = $sce;

        this.isOpen = false;
        this.activeClass = (this.isOpen === true) ? "active" : "";
        this.importantClass = (this.important === true) ? "important" : "";

        $scope.$on("question:reset", () => {
            this.isOpen = false;
            this.activeClass = (this.isOpen === true) ? "active" : "";
        });
    }

    toggleQuestion() {
        if (!this.isOpen) {
            if (this.question.link) {
                this.$location.path(this.question.link);
                return;
            }
        }


        if (!this.isOpen) { // go open this question
            this.$rootScope.$broadcast("question:reset");
        }

        this.isOpen = !this.isOpen;
        this.activeClass = (this.isOpen === true) ? "active" : "";
    }

    getHtmlAnswer(answer) {
        return this.$sce.trustAsHtml(answer);
    }
}