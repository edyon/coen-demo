import angular from 'angular';

import template from './score.template.html';
import style from './_score.scss';

var scoreModule = angular.module('coen.components.score', []);

class ScoreController {

    constructor($scope, DataService) {
        DataService.checkUser().then(user => {
            this.user = user;
        });
    }

    $onInit() {

    }
}

scoreModule.component('profileScore', {
    template: template,
    controller: ScoreController,
    bindings: {
        score: '='
    }
});

export default scoreModule;
