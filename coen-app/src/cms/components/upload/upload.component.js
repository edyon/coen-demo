import angular from 'angular';

import template from './upload.template.html';
import controller from './upload.controller';

var uploadModule = angular.module('cms.components.upload', []);

uploadModule.component('uploadFile', {
    template: template,
    controller: controller,
    bindings: {
        questionId: '='
    }
});

export default uploadModule;
