export default class UploadController {

    constructor($scope, $element, $rootScope, $routeParams, $sce, $window, $timeout, FileUploader) {
        this.$scope = $scope;
        this.$element = $element;
        this.$rootScope = $rootScope;
        this.$timeout = $timeout;
        this.$window = $window;
        this.$sce = $sce;

        this.FileUploader = FileUploader;

        this.campaignId = $routeParams.campaignId;
        this.progress = 0;

        $scope.$watch("$ctrl.questionId", () => {
            if (this.questionId) {
                this.setUrl();
            }
        });
    }

    setUrl () {
        this.uploadUrl = this.parseActionUrl();

        if (this.uploadUrl) {
            this.uploader = new this.FileUploader({
                url: this.uploadUrl,
                withCredentials: true,
                method: 'POST',
                removeAfterUpload: true
            });
// console.log("url = ", this.uploadUrl, " question = ", this.questionId);
            this.initUpload();
        }
    }

    initUpload() {
        this.uploader.onWhenAddingFileFailed = (item /*{File|FileLikeObject}*/, filter, options) => {
// console.info('onWhenAddingFileFailed', item, filter, options);
        };
        this.uploader.onAfterAddingFile = (fileItem) => {
// console.info('onAfterAddingFile', fileItem);
        };
        this.uploader.onAfterAddingAll = (addedFileItems) => {
// console.info('onAfterAddingAll', addedFileItems);
        };
        this.uploader.onBeforeUploadItem = (item) => {
// console.info('onBeforeUploadItem', item);
        };
        this.uploader.onProgressItem = (fileItem, progress) => {
// console.info('onProgressItem', fileItem, progress);
        };
        this.uploader.onProgressAll = (progress) => {
// console.info('onProgressAll', progress);
        };
        this.uploader.onSuccessItem = (fileItem, response, status, headers) => {
// console.info('onSuccessItem', fileItem, response, status, headers);
        };
        this.uploader.onErrorItem = (fileItem, response, status, headers) => {
// console.info('onErrorItem', fileItem, response, status, headers);
            this.showError = true;
        };
        this.uploader.onCancelItem = (fileItem, response, status, headers) => {
// console.info('onCancelItem', fileItem, response, status, headers);
        };
        this.uploader.onCompleteItem = (fileItem, response, status, headers) => {
// console.info('onCompleteItem', fileItem, response, status, headers);
        };
        this.uploader.onCompleteAll = () => {
// console.info('onCompleteAll');
            this.$window.location.reload();
        };
    }

    parseActionUrl() {
        return Config.apiUrl + "/cms/campaign/" + this.campaignId + "/question/" + this.questionId + "/image";
    };

    refreshPage() {
        // this.$timeout(() => {
        //     this.$window.location.reload();
        // }, 1000);
    };

    getPercentage() {
        return (100 / this.progress).toFixed(2);
    }

    handleProgress(progress) {
        this.progress = progress;
    }

    handleComplete(data) {
        this.$rootScope.$broadcast("upload:complete");
    }

    isImage (item) {
        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
        return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
    }
}