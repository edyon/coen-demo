import angular from 'angular';

import controller from './thumb.controller';

var thumbModule = angular.module('cms.components.thumb', []);

thumbModule.component('imageThumb', {
    template: `<img id='image' alt='{{ $ctrl.altText }}' title='{{ $ctrl.title }}'/>`,
    controller: controller,
    bindings: {
        imageUrl: '=',
        imageId: '=',
        altText: '@?',
        title: '@?'
    }
});

export default thumbModule;
