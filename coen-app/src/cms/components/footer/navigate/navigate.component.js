import angular from 'angular';

import template from './navigate.template.html';
import controller from './navigate.controller';

var navigateModule = angular.module('cms.components.navigate', []);

navigateModule.component('footerNavigate', {
    template: template,
    controller: controller,
    bindings: {
        'menu': '='
    }
});

export default navigateModule;
