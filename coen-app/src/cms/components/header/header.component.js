import angular from 'angular';

import template from './header.template.html';
import controller from './header.controller';
import style from './header.scss';

var headerModule = angular.module('cms.components.header', []);

headerModule.component('headerComponent', {
    template: template,
    controller: controller
});

export default headerModule;
