export default class HeaderController {

    constructor(ConfigService) {
        this.showCoen = ConfigService.checkShowCoen();
        this.showBeeldenstorm = ConfigService.checkShowBeeldenstorm();
    }

}