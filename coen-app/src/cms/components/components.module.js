import angular from 'angular';

import app from './app/app.component';
import footer from './footer/footer.component';
import header from './header/header.component';
import thumb from './thumb/thumb.component';
import upload from './upload/upload.component';

const ComponentsModule = angular.module('cms.components', [
    app.name,
    footer.name,
    header.name,
    thumb.name,
    upload.name
]);

export default ComponentsModule;
