import angular from 'angular';

import template from './app.template.html';
import controller from './app.controller';

var appModule = angular.module('cms.components.app', []);

appModule.component('cmsApplication', {
    template: template,
    controller: controller
});

export default appModule;
