export default class CampaignController {

    constructor($scope, $rootScope, $location, $routeParams, RestService) {
        this.$rootScope = $rootScope;
        this.$location = $location;
        this.$routeParams = $routeParams;

        this.RestService = RestService;

        this.getCampaigns();

        $scope.$on('navigate:add', (event, args) => {
            let route = args.route;
            if (route == 'campaign') {
                this.campaign = {};
                this.$rootScope.$broadcast('show:save');
            }
        });

        $scope.$on('navigate:save', (event, args) => {
            let route = args.route;
            if (route == 'campaign') {
                this.goSave();
            }
        });

        $scope.$on('navigate:back', (event, args) => {
            this.navigateBack(args.route);
        });
    }

    getCampaigns() {
        let campaignId = this.$routeParams.campaignId;

        this.RestService.getCampaigns().then(campaigns => {
            this.campaigns = campaigns;

            if (this.campaigns.length == 0) {
                this.campaign = {};
            } else {
                if (campaignId) {
                    this.campaign = _.find(this.campaigns, (campaign) => {
                        return campaign.id == campaignId;
                    });
                } else {
                    this.campaign = null;
                }
            }
        })
    }

    editCampaign(index) {
        this.campaign = this.campaigns[index];
        this.$location.path("campaign/" + this.campaign.id);
    }

    selectCampaign(index) {
        let campaign = this.campaigns[index];
        this.$location.path("campaign/" + campaign.id + "/menu/campaign");
    }

    goSave() {
        if (this.campaign.id && this.campaign.id != "") {
            this.RestService.updateCampaign(this.campaign).then(() => {
                this.$location.path("campaign");
            });
        } else {
            this.RestService.insertCampaign(this.campaign).then(() => {
                this.$location.path("campaign");
            });
        }
    };

    navigateBack(route) {
        if (route == 'campaign') {
            if (this.campaign) {
                this.$location.path("campaign");
            } else {
                this.$location.path("login");
            }
        }
    }
}
