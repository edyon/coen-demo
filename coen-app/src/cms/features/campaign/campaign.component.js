import angular from 'angular';

import template from './campaign.html';
import controller from './campaign.controller';
import style from './_campaign.scss';

var campaignModule = angular.module('cms.features.campaign', []);

campaignModule.component('campaignFeature', {
    template: template,
    controller: controller
});

export default campaignModule;
