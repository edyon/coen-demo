import angular from 'angular';

import template from './question.html';
import controller from './question.controller';
import style from './_question.scss';

var questionModule = angular.module('cms.features.question', []);

questionModule.component('questionFeature', {
    template: template,
    controller: controller
});

export default questionModule;
