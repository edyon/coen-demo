export default class QuestionController {

    constructor($scope, $rootScope, $routeParams, $location, RestService, DataService) {
        this.$rootScope = $rootScope;
        this.$location = $location;

        this.RestService = RestService;

        this.statements = [];
        this.isLoading = false;
        this.isSaving = false;

        this.campaignId = $routeParams.campaignId;
        this.statementId = $routeParams.statementId;

        this.newStatement = {type: "statement", infoPopup: false, choices: []};
        this.getCampaign();

        $scope.$on('navigate:add', (event, args) => {
            var route = args.route;
            if (route == 'statement') {
                this.statement = this.newStatement;
                this.description = "";
                this.$rootScope.$broadcast('show:save');
            }
        });

        $scope.$on('navigate:save', (event, args) => {
            var route = args.route;
            if (route == 'statement') {
                this.goSave();
            }
        });

        $scope.$on('navigate:back', (event, args) => {
            var route = args.route;
            if (route == 'statement') {
                if (this.statement) {
                    this.statement = null;
                    this.$location.path("campaign/" + this.campaignId + "/statement");
                } else {
                    this.$location.path("campaign/" + this.campaignId + "/menu/campaign");
                }
            }
        });

        $scope.$watch('description.information', () => {
            this.updateDescription();
        });

        $scope.$watch('description.link', () => {
            this.updateDescription();
        });

        $scope.$watch('description.decision', () => {
            this.updateDescription();
        });
    }

    getCampaign() {
        if (this.campaignId && this.campaignId != "") {
            this.isLoading = true;

            if (!this.campaign) {
                this.RestService.getCampaign(this.campaignId).then((campaign) => {
                    this.campaign = campaign;

                    this.RestService.getThemes().then((themes) => {
                        this.themes = themes;
                        this.setStatement();
                    });

                });
            } else {
                this.setStatement();
            }
        }
    }

    setStatement() {
        if (this.statementId && this.statementId != "") {
            this.statement = _.find(this.campaign.questions, (item) => {
                return item.id == this.statementId;
            });

            if (this.statement.info) {
                //this.setDescription(this.statement.info);
            }
        }

        this.isLoading = false;
    }

    editStatement($index) {
        this.statement = this.campaign.questions[$index];
        this.$location.path("campaign/" + this.campaignId + "/statement/" + this.statement.id);
    }

    setDescription(description) {
        description = description.split("</br>").join("");
        this.description.information = description.substring(description.indexOf("<p>") + 3, description.indexOf("</p>"));
        description = description.substring(description.indexOf("</p>") + 4, description.length);

        this.description.link = description.substring(description.indexOf("<span>") + 6, description.indexOf("</span>"));
        description = description.substring(description.indexOf("</span>") + 7, description.length);

        this.description.decision = description.substring(description.indexOf("<span>") + 6, description.indexOf("</span>"));
//console.log("set description = ", this.description);
    }

    updateDescription() {
        if (this.statement) {
//console.log("update description = ", this.description);
            var description = "";
            if (this.description.information) {
                description = description + "<p>" + this.description.information + "</p>";
            }
            if (this.description.link) {
                description = description + "<span></br>" + this.description.link + "</br></br></span>";
            }
            if (this.description.decision) {
                description = description + "<p>Besluitvorming</p>";
                description = description + "<span>" + this.description.decision + "</span>";
            }

            this.statement.description = description;
//console.log("update statement = ", $scope.statement);
        }
    }

    /**
     * handle saving the question
     */
    goSave() {
        this.isSaving = true;

        //this.RestService.updateTheme(this.theme).then(() => {
        this.saveStatement();
        //});
    }

    saveStatement() {
        if (this.campaign) {
            if (!this.campaign.questions) this.campaign.questions = [];

            let isNew = false;
            let statementId = Date.now();
            if (!this.statement.id || this.statement.id == "") {
                isNew = true;
                this.statement.id = statementId;
                // this.statement.campaignId = this.campaignId;
                this.campaign.questions.push(this.statement);
            }

            this.RestService.updateCampaign(this.campaign).then(() => {
                this.isSaving = false;

                if (isNew) {
                    this.$location.path("campaign/" + this.campaignId + "/statement/" + statementId);
                }
            });
        }
    }

    deleteStatement(index) {
        this.isSaving = true;
        this.campaign.questions.splice(index, 1);

        this.RestService.updateCampaign(this.campaign).then(() => {
            this.isSaving = false;
        });
    };

    submitForm(event) {
        event.preventDefault();
        return false;
    }

    parseImageUrl(imageId) {
        var fileType = Config.client == "spov" ? ".png" : ".jpg";
        return imageId ? "question-" + imageId + fileType : "";
    };
}
