export default class LoginController {

    constructor($scope, $location, RestService) {
        this.$location = $location;
        this.RestService = RestService;

        this.resetError();
        this.user = {
            _id: 0,
            username: null,
            password: null
        };

        $scope.$on('keypress:enter', () => {
            this.goLogin();
        });
    }

    login () {
        this.goLogin();
    };

    resetError () {
        this.showError = false;
        this.errorText = "";
    }

    goLogin () {
        this.resetError();

        this.RestService.loginPerson(this.user).then((user) => {
            if (user) {
                this.user = user;
                this.$location.path("campaign");
            }
        }).catch(error => {
            if (error.status = -1) {
                this.showError = true;
                this.errorText = "Gebruiker niet gevonden";
            }
        });
    };


}
