import angular from 'angular';

import template from './login.html';
import controller from './login.controller';
import style from './_login.scss';

var loginModule = angular.module('cms.features.login', []);

loginModule.component('loginFeature', {
    template: template,
    controller: controller
});

export default loginModule;
