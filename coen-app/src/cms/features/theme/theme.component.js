import angular from 'angular';

import template from './theme.html';
import controller from './theme.controller';
import style from './theme.scss';

var themeModule = angular.module('cms.features.theme', []);

themeModule.component('themeFeature', {
    template: template,
    controller: controller
});

export default themeModule;
