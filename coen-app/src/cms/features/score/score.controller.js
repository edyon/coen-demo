export default class ScoreController {

    constructor($scope, $location, $routeParams, RestService) {
        this.$location = $location;

        this.RestService = RestService;

        this.campaignId = $routeParams.campaignId;
        this.getAllResults();

        $scope.$on('navigate:back', (event, args) => {
            var route = args.route;
            if (route == 'score') {
                this.$location.path("campaign/" + this.campaignId + "/menu/result");
            }
        });
    }

    getAllResults() {
        this.RestService.getCampaignScores(this.campaignId).then((scores) => {
            this.scores = _.filter(scores, (item) => {
                return item !== null && item !== "null";
            });

            // handle old results without campaignId
            if (this.campaignId == Config.campaignId) {
                this.RestService.getScores().then((oldScores) => {
                    _.each(oldScores, (item) => {
                        if (item && item.campaignId == null) {
                            item.campaignId = this.campaignId;
                            this.scores.push(item);
                        }
                    });
                });
            }
        });
    };

    selectWinner() {
        let winners = _.filter(this.scores, (item) => {
            return item.scoreCount === 15;
        });

        if (winners.length == 0) {
            winners = _.filter(this.scores, (item) => {
                return item.scoreCount === 14;
            });
        }

        if (winners.length == 0) {
            winners = _.filter(this.scores, (item) => {
                return item.scoreCount > 10;
            });
        }

        let randomWinner = Math.floor(Math.random() * winners.length);
        this.winner = winners[randomWinner];
    };
}
