import angular from 'angular';

import campaign from './campaign/campaign.component';
import login from './login/login.component';
import menu from './menu/menu.component';
import person from './person/person.component';
import question from './question/question.component';
import result from './result/result.component';
import resultFiltered from './result-filtered/result-filtered.component';
import score from './score/score.component';
import theme from './theme/theme.component';

const FeaturesModule = angular.module('cms.features', [
    campaign.name,
    login.name,
    menu.name,
    person.name,
    question.name,
    result.name,
    resultFiltered.name,
    score.name,
    theme.name
]);

export default FeaturesModule;
