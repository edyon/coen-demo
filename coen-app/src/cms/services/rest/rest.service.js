class RestService {

    constructor($http, $q, $location) {
        this.$http = $http;
        this.$q = $q;
        this.$location = $location;

        this._apiUrl = Config.apiUrl;
    }

    handleError(data, status) {
        if (status == -1) {
            this.$location.path('inloggen');
        }
    };

    // login admin
    loginPerson(person) {
        var deferred = this.$q.defer();
        this.$http.post(this._apiUrl + "/admin/login", person, {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            deferred.reject(data);
        });
        return deferred.promise;
    };

    // logout admin
    logoutPerson() {
        var deferred = this.$q.defer();
        this.$http.get(this._apiUrl + "/admin/logout", {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            deferred.reject(data);
        });
        return deferred.promise;
    };

    // check user
    getLoggedUser() {
        var deferred = this.$q.defer();
        this.$http.get(this._apiUrl + "/admin", {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            deferred.reject(data);
        });
        return deferred.promise;
    };

    // get campaigns
    getCampaigns() {
        var deferred = this.$q.defer();
        this.$http.get(this._apiUrl + "/cms/campaign/all", {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            this.handleError(data);
            deferred.reject(data);
        });
        return deferred.promise;
    };

    // get campaign
    getCampaign(campaignId) {
        var deferred = this.$q.defer();
        this.$http.get(this._apiUrl + "/cms/campaign/" + campaignId, {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            this.handleError(data);
            deferred.reject(data);
        });
        return deferred.promise;
    };

    // insert campaign
    insertCampaign(campaign) {
        var deferred = this.$q.defer();
        this.$http.post(this._apiUrl + "/cms/campaign", campaign, {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            this.handleError(data);
            deferred.reject(data);
        });
        return deferred.promise;
    };

    // update campaigns
    updateCampaign(campaign) {
        var deferred = this.$q.defer();
        this.$http.put(this._apiUrl + "/cms/campaign/" + campaign.id, campaign, {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            this.handleError(data);
            deferred.reject(data);
        });
        return deferred.promise;
    };

    // delete campaign
    deleteCampaign(campaign) {
        var deferred = this.$q.defer();
        this.$http.delete(this._apiUrl + "/cms/campaign/" + campaign.id, {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            this.handleError(data);
            deferred.reject(data);
        });
        return deferred.promise;
    };

    // get themes
    getThemes() {
        var deferred = this.$q.defer();
        this.$http.get(this._apiUrl + "/cms/topic/all", {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            this.handleError(data);
            deferred.reject(data);
        });
        return deferred.promise;
    };

    // get questions
    getQuestionsByCampaign(campaignId) {
        var deferred = this.$q.defer();
        this.$http.get(this._apiUrl + "/cms/campaign/" + campaignId + "/question", {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            this.handleError(data);
            deferred.reject(data);
        });
        return deferred.promise;
    };

    // get theme
    getTheme(themeId) {
        var deferred = this.$q.defer();
        this.$http.get(this._apiUrl + "/cms/topic/" + themeId, {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            this.handleError(data);
            deferred.reject(data);
        });
        return deferred.promise;
    };

    // insert theme
    insertTheme(theme) {
        var deferred = this.$q.defer();
        this.$http.post(this._apiUrl + "/cms/topic", theme, {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            this.handleError(data);
            deferred.reject(data);
        });
        return deferred.promise;
    };

    // update theme
    updateTheme(theme) {
        var deferred = this.$q.defer();
        this.$http.put(this._apiUrl + "/cms/topic/" + theme.id, theme, {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            this.handleError(data);
            deferred.reject(data);
        });
        return deferred.promise;
    };

    // delete theme
    deleteTheme(theme) {
        var deferred = this.$q.defer();
        this.$http.delete(this._apiUrl + "/cms/topic/" + theme.id, {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            this.handleError(data);
            deferred.reject(data);
        });
        return deferred.promise;
    };

    // get statement
    getStatement(statementId) {
        var deferred = this.$q.defer();
        this.$http.get(this._apiUrl + "/cms/question/" + statementId, {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            this.handleError(data);
            deferred.reject(data);
        });
        return deferred.promise;
    };

    // insert question in campaign
    insertQuestion(campaignId, question) {
        var deferred = this.$q.defer();
        this.$http.post(this._apiUrl + "/cms/campaign/" + campaignId + "/question", question, {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            this.handleError(data);
            deferred.reject(data);
        });
        return deferred.promise;
    };

    // update statement
    updateStatement(statement) {
        var deferred = this.$q.defer();
        this.$http.put(this._apiUrl + "/cms/question/" + statement.id, statement, {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            this.handleError(data);
            deferred.reject(data);
        });
        return deferred.promise;
    };

    // delete statement
    deleteStatement(statement) {
        var deferred = this.$q.defer();
        this.$http.delete(this._apiUrl + "/cms/question/" + statement.id, {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            this.handleError(data);
            deferred.reject(data);
        });
        return deferred.promise;
    };

    // get scores
    getScores() {
        var deferred = this.$q.defer();
        this.$http.get(this._apiUrl + "/cms/scores", {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            this.handleError(data);
            deferred.reject(data);
        });
        return deferred.promise;
    };

    // get scores by campaign
    getCampaignScores(campaignId) {
        var deferred = this.$q.defer();
        this.$http.get(this._apiUrl + "/cms/campaign/" + campaignId + "/scores", {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            this.handleError(data);
            deferred.reject(data);
        });
        return deferred.promise;
    };

    // get results
    getResults() {
        var deferred = this.$q.defer();
        this.$http.get(this._apiUrl + "/cms/results", {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            this.handleError(data);
            deferred.reject(data);
        });
        return deferred.promise;
    };

    // get results by campaign
    getCampaignResults(campaignId) {
        var deferred = this.$q.defer();
        this.$http.get(this._apiUrl + "/cms/campaign/" + campaignId + "/results", {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            this.handleError(data);
            deferred.reject(data);
        });
        return deferred.promise;
    };

    // get all persons
    getPersons(personId) {
        var deferred = this.$q.defer();
        this.$http.get(this._apiUrl + "/cms/person/all", {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            this.handleError(data);
            deferred.reject(data);
        });
        return deferred.promise;
    };

    // get results for a person
    getPersonResults(personId) {
        var deferred = this.$q.defer();
        this.$http.get(this._apiUrl + "/cms/result/" + personId, {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            this.handleError(data);
            deferred.reject(data);
        });
        return deferred.promise;
    };

    static RestFactory($http, $q, $location) {
        return new RestService($http, $q, $location);
    }
}

RestService.RestFactory.$inject = ['$http', '$q', '$location'];

angular.module('cms.services.rest', [])
    .factory('RestService', RestService.RestFactory);

export default RestService;