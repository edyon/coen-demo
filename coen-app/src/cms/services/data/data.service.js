class DataService {

    constructor($rootScope, $q, $location, RestService) {
        this.$rootScope = $rootScope;
        this.$q = $q;
        this.$location = $location;

        this.RestService = RestService;

        this._user = null;
    }

    checkAdmin() {
        var deferred = this.$q.defer();
        if (!this._user) {
            this.RestService.getLoggedUser().then((user) => {
                var success = user && user != "null";
                if (success) {
                    this.setAdmin(user);
                }
                deferred.resolve(user);

            }).catch(error => {
                this.setAdmin(null);
                deferred.resolve(null);
                this.$location.path("inloggen");
            });
        } else {
            deferred.resolve(this._user);
        }
        return deferred.promise;
    };

    setAdmin(user) {
        this._user = user;
        this.$rootScope.$broadcast('user', {user: this._user});
    };

    getAdmin() {
        return this._user;
    };

    static DataFactory($rootScope, $q, $location, RestService) {
        return new DataService($rootScope, $q, $location, RestService);
    }
}

DataService.DataFactory.$inject = ['$rootScope', '$q', '$location', 'RestService'];

angular.module('cms.services.data', [])
    .factory('DataService', DataService.DataFactory);

export default DataService;