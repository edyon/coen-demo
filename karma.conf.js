module.exports = function (config) {
    'use strict';

    config.set({
        basePath: '',

        // frameworks to use
        // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
        frameworks: ['jasmine', 'jasmine-matchers'],

        files: [
            'node_modules/babel-polyfill/dist/polyfill.js',
            'dist/js/default-istanbul.js',
            'test/libs/angular-mocks.js',
            'test/libs/angular-resource.js',
            'test/libs/underscore.js',
            'test/libs/karma-read-json.js',
            'coen-app/src/app/**/*.spec.js',
            {pattern: 'mocks/**/*.json', included: false}
        ],

        preprocessors: {
            'coen-app/src/app/**/!(test)/*.js': ['webpack', 'coverage'],
            'coen-app/src/app/**/*.spec.js': ['webpack']
        },

        babelPreprocessor: {
            options: {
                presets: ['es2015'],
                sourceMap: 'inline'
            }
        },

        webpack: {
            module: {
                loaders: [
                    { test: /\.js/, exclude: /(node_modules|dist)/, loader: 'babel-loader'}
                ],
                postLoaders: [
                    { test: /\.js/, exclude: /(node_modules|dist|.[sS]pec\.js$)/, loader: 'istanbul-instrumenter'}
                ]
            },
            watch: true
        },

        webpackServer: {
            noInfo: true
        },

        // test results reporter to use
        // possible values: 'dots', 'progress'
        // available reporters: https://npmjs.org/browse/keyword/karma-reporter
        reporters: ['progress', 'html', 'coverage', 'threshold'],

        // web server port
        port: 9876,

        // cli runner port
        runnerPort: 9100,

        // enable / disable colors in the output (reporters and logs)
        colors: true,

        // level of logging
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,

        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: true,

        // Which plugins to enable
        plugins: [
            'karma-babel-preprocessor',
            'karma-phantomjs-launcher',
            'karma-chrome-launcher',
            'karma-webpack',
            'karma-coverage',
            'karma-jasmine',
            'karma-jasmine-matchers',
            'karma-threshold-reporter',
            'karma-html-reporter',
            'phantomjs-prebuilt'
        ],

        // start these browsers
        // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
        browsers: ['PhantomJS'/*, 'Chrome', 'Firefox', 'Safari', 'Opera', 'IE'*/],

        // Continuous Integration mode
        // if true, Karma captures browsers, runs the tests and exits
        singleRun: false,

        viewportSize: {
            width: 1200,
            height: 768
        },

        captureTimeout: 60000,

        // to avoid DISCONNECTED messages
        browserDisconnectTimeout: 10000, // default 2000
        browserDisconnectTolerance: 1, // default 0
        browserNoActivityTimeout: 60000, //default 10000

        // optionally, configure the reporter
        coverageReporter: {
            reporters: [
                {type: 'cobertura', subdir: '.', file: 'cobertura.xml'},
                {type: 'html', subdir: 'report-html'}
            ],
            dir: 'test/results/coverage/'
        },
        thresholdReporter: {
            statements: 0,
            branches: 0,
            functions: 0,
            lines: 0
        },
        htmlReporter: {
            outputDir: 'test/results/jasmine/'
        }

    });
};
