var gulp = require('gulp'),
	plugins = require('gulp-load-plugins')();

// app
gulp.task('html', ['html:app', 'html:cms']);

gulp.task('html:app', function () {
	return gulp.src('coen-app/index.html')
		.pipe(plugins.minifyHtml())
		.pipe(gulp.dest('./dist/'));
});

gulp.task('html:cms', function () {
    return gulp.src('coen-app/cms.html')
        .pipe(plugins.minifyHtml())
        .pipe(gulp.dest('./dist/'));
});

// coen
gulp.task('html:coen', ['html:coen:app', 'html:coen:cms']);

gulp.task('html:coen:app', function () {
    return gulp.src('coen-app/index/coen.html')
        .pipe(plugins.minifyHtml())
        .pipe(plugins.rename("index.html"))
        .pipe(gulp.dest('./dist/'));
});

gulp.task('html:coen:cms', function () {
    return gulp.src('coen-app/index/coen-cms.html')
        .pipe(plugins.minifyHtml())
        .pipe(plugins.rename("index-cms.html"))
        .pipe(gulp.dest('./dist/'));
});

// hardenberg
gulp.task('html:hardenberg', ['html:hardenberg:app', 'html:hardenberg:cms']);

gulp.task('html:hardenberg:app', function () {
    return gulp.src('coen-app/index/hardenberg.html')
        .pipe(plugins.minifyHtml())
        .pipe(plugins.rename("index.html"))
        .pipe(gulp.dest('./dist/'));
});

gulp.task('html:hardenberg:cms', function () {
    return gulp.src('coen-app/index/hardenberg-cms.html')
        .pipe(plugins.minifyHtml())
        .pipe(plugins.rename("index-cms.html"))
        .pipe(gulp.dest('./dist/'));
});

// roc
gulp.task('html:roc', ['html:roc:app', 'html:roc:cms']);

gulp.task('html:roc:app', function () {
    return gulp.src('coen-app/index/roc.html')
        .pipe(plugins.minifyHtml())
        .pipe(plugins.rename("index.html"))
        .pipe(gulp.dest('./dist/'));
});

gulp.task('html:roc:cms', function () {
    return gulp.src('coen-app/index/roc-cms.html')
        .pipe(plugins.minifyHtml())
        .pipe(plugins.rename("index-cms.html"))
        .pipe(gulp.dest('./dist/'));
});

// verkiezingen
gulp.task('html:verkiezingen', ['html:verkiezingen:app', 'html:verkiezingen:cms']);

gulp.task('html:verkiezingen:app', function () {
    return gulp.src('coen-app/index/verkiezingen.html')
        .pipe(plugins.minifyHtml())
        .pipe(plugins.rename("index.html"))
        .pipe(gulp.dest('./dist/'));
});

gulp.task('html:verkiezingen:cms', function () {
    return gulp.src('coen-app/index/verkiezingen-cms.html')
        .pipe(plugins.minifyHtml())
        .pipe(plugins.rename("index-cms.html"))
        .pipe(gulp.dest('./dist/'));
});

// spov
gulp.task('html:spov', ['html:spov:app', 'html:spov:cms']);

gulp.task('html:spov:app', function () {
    return gulp.src('coen-app/index/spov.html')
        .pipe(plugins.minifyHtml())
        .pipe(plugins.rename("index.html"))
        .pipe(gulp.dest('./dist/'));
});

gulp.task('html:spov:cms', function () {
    return gulp.src('coen-app/index/spov-cms.html')
        .pipe(plugins.minifyHtml())
        .pipe(plugins.rename("index-cms.html"))
        .pipe(gulp.dest('./dist/'));
});