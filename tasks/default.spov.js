import gulp from 'gulp';
import util from 'gulp-util';

/*
 ** Run App
 */
gulp.task('run:spov', ['spov:synced'], function () {
    gulp.watch(['coen-app/src/app/**'], ['js:build:spov']);
    gulp.watch(['coen-app/src/cms/**'], ['js:build:cms']);
    gulp.watch(['coen-app/index/spov.html', 'coen-app/index/spov-cms.html'], ['html:spov']);
    gulp.watch(['locale/nl/spov.json'], ['copy:translations:spov']);
    gulp.watch(['locale/nl/cms.json'], ['copy:translations:css']);
});

/*
 ** Build App
 */
gulp.task('spov:synced', ['clean'], function () {
    util.log('--> clean task executed.');
    gulp.start('spov:build');
});

gulp.task('spov:build', ['html:spov', 'js:build:spov', 'js:build:cms'], function () {
    util.log('--> build task executed.');
    gulp.start('spov:copy');
});

gulp.task('spov:copy', ['copy', 'copy:translations:spov', 'copy:translations:cms'], function () {
    util.log('--> copy task executed.');
    gulp.start('spov:run');
});

gulp.task('spov:run', ['connect'], function () {
    util.log('--> connect task executed.');
});

/*
 ** Build App Release (called from Jenkins)
 */
gulp.task('spov:release', ['clean'], function () {
    util.log('--> clean task executed.');
    gulp.start('spov:build:prod');
});

gulp.task('spov:build:prod', ['html:spov', 'js:build:spov:prod', 'js:build:cms:prod'], function () {
    util.log('--> build task executed.');
    gulp.start('spov:copy:prod');
});

gulp.task('spov:copy:prod', ['copy', 'copy:translations:spov', 'copy:translations:cms'], function () {
    util.log('--> copy task executed.');
});

/*
 ** Build Mobile App
 */
gulp.task('spov:mobile', ['clean'], function () {
    util.log('--> clean task executed.');
    gulp.start('spov:build:mobile');
});

gulp.task('spov:build:mobile', ['html:spov:app', 'js:build:spov'], function () {
    util.log('--> build task executed.');
    gulp.start('spov:copy:mobile');
});

gulp.task('spov:copy:mobile', ['copy:translations:spov'], function () {
    util.log('--> copy task executed.');
    gulp.start('spov:www');
});

gulp.task('spov:www', ['copy:mobile:spov'], function () {
    util.log('--> www task executed.');
});


/*
 ** Copy Mobile App
 */
gulp.task('copy:mobile:spov', ['copy:app:mobile', 'copy:mobile:images:spov', 'copy:mobile:config:spov', 'copy:mobile:locale:spov']);

gulp.task('copy:mobile:config:spov', function () {
    gulp.src(['locale/**/spov.json'])
        .pipe(gulp.dest('./coen-mobile/www/locale'));
});

gulp.task('copy:mobile:images:spov', function () {
    gulp.src(['coen-app/assets/img/logos/spov/icon.png'])
        .pipe(gulp.dest('./coen-mobile/www'));
    gulp.src(['coen-app/assets/img/mobile/res-spov/**'])
        .pipe(gulp.dest('./coen-mobile/www/img/res'));
});

gulp.task('copy:mobile:locale:spov', function () {
    gulp.src(['coen-app/assets/mobile-config/spov/config.xml'])
        .pipe(gulp.dest('./coen-mobile'));
});