import gulp from 'gulp';
import karma from 'karma';

gulp.task('test', ['copy:testLib', 'test:unit']);

gulp.task('test:unit', ['js:build:istanbul'], (done) => {
	new karma.Server({
		configFile: `${__dirname} /../karma.conf.js`,
		singleRun: true
	}, function() {
		done();
	}).start();
});

gulp.task('test:tdd', ['js:build:istanbul'], (done) => {
	new karma.Server({
		configFile: `${__dirname} /../karma.conf.js`,
		singleRun: false
	}, function() {
		done();
	}).start();
});
