import gulp from 'gulp';
import util from 'gulp-util';

/*
 ** Run App
 */
gulp.task('run:roc', ['roc:synced'], function () {
    gulp.watch(['coen-app/src/**'], ['clean:js', 'js:build:roc', 'js:build:cms']);
    gulp.watch(['coen-app/index/roc.html', 'coen-app/index/roc-cms.html'], ['html:roc']);
    gulp.watch(['locale/nl/roc.json'], ['copy:translations:roc']);
});

/*
 ** Build App
 */
gulp.task('roc:synced', ['clean'], function () {
    util.log('--> clean task executed.');
    gulp.start('roc:build');
});

gulp.task('roc:build', ['html:roc', 'js:build:roc', 'js:build:cms'], function () {
    util.log('--> build task executed.');
    gulp.start('roc:copy');
});

gulp.task('roc:copy', ['copy', 'copy:translations:cms'], function () {
    util.log('--> copy task executed.');
    gulp.start('roc:run');
});

gulp.task('roc:run', ['connect'], function () {
    util.log('--> connect task executed.');
});

/*
 ** Build App Release (called from Jenkins)
 */
gulp.task('roc:release', ['clean'], function () {
    util.log('--> clean task executed.');
    gulp.start('roc:build:prod');
});

gulp.task('roc:build:prod', ['html:roc', 'js:build:roc:prod', 'js:build:cms:prod'], function () {
    util.log('--> build task executed.');
    gulp.start('roc:copy:prod');
});

gulp.task('roc:copy:prod', ['copy', 'copy:translations:roc', 'copy:translations:cms'], function () {
    util.log('--> copy task executed.');
});