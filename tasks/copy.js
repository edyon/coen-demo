var gulp = require('gulp'),
    plugins = require('gulp-load-plugins')();

gulp.task('copy', ['copy:testLib', 'copy:translations']);

// copy test libs
gulp.task('copy:testLib', function () {
    gulp.src('node_modules/angular/angular.js')
        .pipe(gulp.dest('./test/libs/'));
    gulp.src('node_modules/angular-mocks/angular-mocks.js')
        .pipe(gulp.dest('./test/libs/'));
    gulp.src('node_modules/angular-resource/angular-resource.js')
        .pipe(gulp.dest('./test/libs/'));
    gulp.src('node_modules/karma-read-json/karma-read-json.js')
        .pipe(gulp.dest('./test/libs/'));
    gulp.src('node_modules/underscore/underscore.js')
        .pipe(gulp.dest('./test/libs/'));
});

// copy translations
gulp.task('copy:translations', ['copy:translations:coen', 'copy:translations:hardenberg', 'copy:translations:roc', 'copy:translations:verkiezingen']);

gulp.task('copy:translations:coen', function () {
    gulp.src(['locale/nl/coen.json'])
        .pipe(gulp.dest('./dist/locale/nl'));
});

gulp.task('copy:translations:hardenberg', function () {
    gulp.src(['locale/nl/hardenberg.json'])
        .pipe(gulp.dest('./dist/locale/nl'));
});

gulp.task('copy:translations:roc', function () {
    gulp.src(['locale/nl/roc.json'])
        .pipe(gulp.dest('./dist/locale/nl'));
});

gulp.task('copy:translations:verkiezingen', function () {
    gulp.src(['locale/nl/verkiezingen.json'])
        .pipe(gulp.dest('./dist/locale/nl'));
});

gulp.task('copy:translations:spov', function () {
    gulp.src(['locale/nl/spov.json'])
        .pipe(gulp.dest('./dist/locale/nl'));
});

gulp.task('copy:translations:cms', function () {
    gulp.src(['locale/nl/cms.json'])
        .pipe(gulp.dest('./dist/locale/nl'));
});

gulp.task('copy:app:mobile', function () {
    gulp.src(['dist/**'])
        .pipe(gulp.dest('./coen-mobile/www'));
});