import gulp from 'gulp';
import gulpLoadPlugins from 'gulp-load-plugins';

const $ = gulpLoadPlugins();

gulp.task('connect', () => {
    var stream = $.nodemon({
        script: './server/server.babel.js',
        ext: 'js html',
        env: {
            'NODE_ENV': 'development',
            'PORT': process.env.E2E_PORT || process.env.PORT || 8000
        },
        watch: [
            'server/*',
            'server/**/*'
        ]
    });

    stream
        .on('restart', () => {
            console.log('restarted!');
        })
        .on('crash', () => {
            console.error('Application has crashed!\n');
            stream.emit('restart', 2);  // restart the server in 2 seconds
        })
});